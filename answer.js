const fs = require('fs');

// Parse command line arguments
const inputFilePath = process.argv[2];

// Read input file
const inputFileContent = fs.readFileSync(inputFilePath, 'utf-8');

// Split input file into records
const records = inputFileContent.trim().split('\n');

// Group records by satellite ID and component
const groups = {};
for (const record of records) {
  const [timestamp, satelliteId, redHigh, yellowHigh, yellowLow, redLow, rawValue, component] = record.split('|');
  const key = `${satelliteId}-${component}`;
  if (!groups[key]) {
    groups[key] = [];
  }
  groups[key].push({timestamp, satelliteId, redHigh, yellowHigh, yellowLow, redLow, rawValue, component});
}

// Check for violations
const violations = [];
for (const key in groups) {
  const records = groups[key];
  const component = records[0].component;
  const satelliteId = records[0].satelliteId;

  // Initialize variables to track consecutive red low/high readings and last violation timestamp
  let consecutiveRedLowCount = 0;
  let consecutiveRedHighCount = 0;
  let lastViolationTimestamp = null;

  // Iterate through each record for a given satellite ID and component
  for (const record of records) {
    const {timestamp, redHigh, redLow, rawValue} = record;
    
    // Check for consecutive red low readings and add a violation if 3 are found
    if (rawValue < redLow) {
      consecutiveRedLowCount++;
      if (consecutiveRedLowCount === 3) {
        violations.push({satelliteId, severity: "RED LOW", component, timestamp: record.timestamp});
        consecutiveRedLowCount = 0;
        lastViolationTimestamp = record.timestamp;
      }
    } else {
      consecutiveRedLowCount = 0;
    }
    // Check for consecutive red high readings and add a violation if 3 are found
    if (rawValue > redHigh) {
      consecutiveRedHighCount++;
      if (consecutiveRedHighCount === 3) {
        violations.push({satelliteId, severity: "RED HIGH", component, timestamp: record.timestamp});
        consecutiveRedHighCount = 0;
        lastViolationTimestamp = record.timestamp;
      }
    } else {
      consecutiveRedHighCount = 0;
    }
    // Update last violation timestamp
    lastViolationTimestamp = record.timestamp;
  }
}

// Print violations as formatted JSON
for (const violation of violations) {
  console.log(JSON.stringify({
    satelliteId: violation.satelliteId,
    severity: violation.severity,
    component: violation.component,
    timestamp: violation.timestamp + "Z"
  }, null, 2));
}
